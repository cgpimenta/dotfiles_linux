#!/bin/bash

read -p "Install essential software? [Y|n] " CONF
CONF=${CONF:-"Y"}
if [ "$CONF" = "y" ] || [ "$CONF" = "Y" ]
then
    echo "Installing essential software..."
    sudo apt install -y build-essential git meld vim neovim terminator htop python3 python3-pip fonts-powerline
fi
echo

read -p "Install bash-it? [Y|n] " CONF
CONF=${CONF:-"Y"}
if [ "$CONF" = "y" ] || [ "$CONF" = "Y" ]
then
    echo "Installing bash-it..."
    git clone --depth=1 https://github.com/Bash-it/bash-it.git ~/.bash_it
    ~/.bash_it/install.sh
fi
echo

read -p "Install Java 8? [Y|n] " CONF
CONF=${CONF:-"Y"}
if [ "$CONF" = "y" ] || [ "$CONF" = "Y" ]
then
    echo "Installing Java 8..."
    sudo add-apt-repository ppa:webupd8team/java
    sudo apt update
    sudo apt install -y oracle-java8-installer
fi
echo

read -p "Install Sublime Text ? [Y|n] " CONF
CONF=${CONF:-"Y"}
if [ "$CONF" = "y" ] || [ "$CONF" = "Y" ]
then
    echo "Installing Sublime Text..."
    wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
    sudo apt install -y apt-transport-https
    echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
    sudo apt update
    sudo apt install -y sublime-text
fi
echo

read -p "Install Tex Live and TeXstudio? [Y|n] " CONF
CONF=${CONF:-"Y"}
if [ "$CONF" = "y" ] || [ "$CONF" = "Y" ]
then
    echo "Installing TeX Live 2018 and TeXstudio..."
    sudo rm -rf /usr/local/texlive/*
    sudo rm -rf ~/.texlive*
    sudo ./install-tl-2018/install-tl
    sudo apt install -y texstudio
fi
echo

read -p "Install Docker? [Y|n] " CONF
CONF=${CONF:-"Y"}
if [ "$CONF" = "y" ] || [ "$CONF" = "Y" ]
then
    sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    sudo apt update
    sudo apt install -y docker-ce

    # Avoid using sudo
    sudo groupadd docker
    sudo usermod -aG docker $USER
fi
echo

read -p "Update bashrc and bash_aliases? [Y|n] " CONF
CONF=${CONF:-"Y"}
if [ "$CONF" = "y" ] || [ "$CONF" = "Y" ]
then
    echo "Updating settings: bashrc, bash_aliases..."
    cp ./bashrc $HOME/.bashrc
    cp ./bash_aliases $HOME/.bash_aliases
fi
echo

read -p "Copy vim and neovim settings? [Y|n] " CONF
CONF=${CONF:-"Y"}
if [ "$CONF" = "y" ] || [ "$CONF" = "Y" ]
then
    echo "Updating settings: vim, neovim..."
    cp ./vimrc $HOME/.vimrc
    rm -rf $HOME/.config/nvim
    cp -r ./nvim $HOME/.config/nvim
    rm -rf $HOME/.config/nvim/bundle/Vundle.vim
    git clone https://github.com/VundleVim/Vundle.vim.git ~/.config/nvim/bundle/Vundle.vim
    read -p "Remember to run 'PluginInstall' in neovim."
fi
echo

read -p "Copy git settings? [Y|n] " CONF
CONF=${CONF:-"Y"}
if [ "$CONF" = "y" ] || [ "$CONF" = "Y" ]
then
    echo "Updating settings: git..."
    cp ./gitconfig $HOME/.gitconfig
    cp ./gitingore-global $HOME/.gitignore-global
fi
echo

echo "** Install Anaconda **"
echo "** Sublime Text settings: package 'PackageSync'. **"
echo
