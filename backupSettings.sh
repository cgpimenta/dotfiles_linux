#!/bin/bash

echo "Copying settings..."
cp $HOME/.bashrc ./bashrc
cp $HOME/.bash_aliases ./bash_aliases
cp $HOME/.vimrc ./vimrc
sudo cp -r $HOME/.config/nvim ./nvim
cp $HOME/.gitconfig ./gitconfig
cp $HOME/.gitignore-global ./gitignore-global
cp $HOME/Documents/disc.sh ./disc.sh
echo

echo "SAVE SUBLIME SETTINGS USING 'Package Syncing'!"
read -p "OK!"
