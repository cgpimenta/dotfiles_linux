#!/usr/bin/env bash

usage()
{
  echo "Usage: "
}

while getopts ":p:a:" OPT; do
  case $OPT in
    p)   PACKAGES=$OPTARG
         ;;
    a)   AUR=$OPTARG
         ;;
    [?]) usage
         exit 1;;
  esac
done
shift $((OPTIND - 1))

read -p "Update pacman mirrors? [Y|n] " CONF
CONF=${CONF:-"Y"}
if [ $CONF = "y" ] || [ $CONF = "Y" ]
then
  sudo pacman-mirrors --country Brazil && sudo pacman -Syyu
fi

if [ ! -z $PACKAGES ]
then
  sudo pacman -Syu --needed - < $PACKAGES
fi

if [ ! -z $AUR ]
then
  for package in `cat $AUR`; do
    pamac build $package
  done
fi

read -p "Copy ZSH config files? [Y|n] " CONF
CONF=${CONF:-"Y"}
if [ $CONF = "y" ] || [ $CONF = "Y" ]
then
  cp ./zshrc ~/.zshrc
  cp ./zsh_aliases ~/.zsh_aliases
  
  sh -c "$(wget -O- https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
  git clone https://github.com/romkatv/powerlevel10k.git $ZSH_CUSTOM/themes/powerlevel10k
  cp ./p10k-lean.zsh ~/.oh-my-zsh/p10k-lean.zsh
fi


read -p "Copy git, vim and tmux config files? [Y|n] " CONF
CONF=${CONF:-"Y"}
if [ $CONF = "y" ] || [ $CONF = "Y" ]
then
  cp ./tmux.conf ~/.tmux.conf
  cp ./gitconfig ~/.gitconfig
  cp ./vimrc ~/.vimrc
  cp -r ./vim ~/.vim

  git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
fi

read -p "Install Sublime Text? [Y|n] " CONF
CONF=${CONF:-"Y"}
if [ $CONF = "y" ] || [ $CONF = "Y" ]
then
  curl -O https://download.sublimetext.com/sublimehq-pub.gpg && sudo pacman-key --add sublimehq-pub.gpg && sudo pacman-key --lsign-key 8A8F901A && rm sublimehq-pub.gpg
  echo -e "\n[sublime-text]\nServer = https://download.sublimetext.com/arch/stable/x86_64" | sudo tee -a /etc/pacman.conf
  sudo pacman -Syu sublime-text
fi
