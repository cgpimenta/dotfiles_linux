#!/bin/bash

DISC=$1

if [ "$DISC" = "paa" ]; then
    DISC="PAA"
fi

if [ "$DISC" = "auto" ] || [ "$DISC" = "automl" ]; then
    DISC="autoML"
fi

cd `echo "${HOME}/Documents/Disc/$DISC"`
